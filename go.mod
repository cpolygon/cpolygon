module gitlab.com/cpolygon/cpolygon

go 1.17

require (
	github.com/valyala/fasthttp v1.29.0
	gitlab.com/cpolygon/common v0.0.0-20210908102036-ab525574512a
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
